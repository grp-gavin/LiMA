"""Setup script."""

from setuptools import setup, find_packages

setup(
    name="lima",
    version="1.2.1",
    description="LiMA Analysis Suite",
    long_description="This is a program to perform file operations and image \
        analysis on images acquired via automated microscopy in a liposome \
        microarray (LiMA) experiment. It is designed to run on EMBL \
        High-Performance Computing Cluster under the SLURM scheduler.",

    url="https://git.embl.de/erkut/LiMA",

    author="Cihan Erkut",
    author_email="cihan.erkut@embl.de",

    license="The clear BSD license",

    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
        "License :: OSI Approved :: The Clear BSD License",
        "Programming Language :: Python :: 3.6",
    ],
    keywords="liposome microarray bioinformatics",

    packages=find_packages(),
    install_requires=["numpy>=1.13.3", "imread>=0.6.1",
                      "PyYAML>=3.12", "Pillow>=5.1.0", "olefile>=0.45.1"],
    python_requires=">=3.6",
    include_package_data=True,
    package_data={
        "lima": ["config/lima.conf",
                 "config/logging.conf",
                 "scripts/slima.sh",
                 "scripts/run.sh",
                 "scripts/create_batch_data.sh",
                 "scripts/submit_jobs.sh",
                 "scripts/run_cellprofiler.sh"]
    },

    entry_points={
        "console_scripts": [
            "lima=lima.__init__:main",
        ],
    },
)
