# Contributing to the project

You can contribute to this project in two ways.

## 1. If you are a user:

### Bug reporting

Test the program and report bugs via creating an [issue][aaad8517]. Do not forget to add a
description of the problem, how you got it and which error messages you got.
Also leave the input files in their location so that we can reproduce the error.
Bugs are priority number one, they will be fixed as soon as possible.

### Feature request

If you think that a new feature should be implemented or an existing feature
should be modified, again use the issue system for giving your suggestions.
Code maintainer(s) will take it into consideration.

## 2. If you are a coder:

Please contact the code maintainer(s) before you take any action. If it is
agreed that you can modify the code, you probably already know how to proceed.

  [aaad8517]: https://git.embl.de/erkut/LiMA/issues/new "Create new issue"
