#!/usr/bin/bash

job_CPBatch=$(sbatch --parsable ./create_batch_data.sh)
sbatch --dependency=afterok:"$job_CPBatch" ./submit_jobs.sh
echo "Check job state with \"jstat\""
