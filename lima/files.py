# Copyright (C) 2017 Cihan Erkut

"""File-related operations."""

import logging
import shutil
import sys

logger = logging.getLogger(__name__)


def confirm_folders(session):
    """Confirm user-provided folders."""
    # Confirm input folder
    if session.input_folder.is_dir():
        logger.info(f"Input folder confirmed: {session.input_folder}")
    else:
        logger.error("Input folder does not exist")
        sys.exit(1)

    # Confirm pipeline file
    if session.pipeline_file.is_file():
        logger.info(f"Pipeline file confirmed: {session.pipeline_file}")
    else:
        logger.error("Pipeline file does not exist")
        sys.exit(1)

    # Create scratch space
    if not session.scratch_folder.is_dir():
        try:
            session.scratch_folder.mkdir(parents=True)
            logger.debug(f"Scratch folder created: {session.scratch_folder}")
        except Exception as e:
            logger.error(f"Cannot create scratch directory! ({e})")
            sys.exit(1)

    # Confirm group share folders
    if session.dest_converted.is_dir():
        logger.info(f"Destage folder for converted images confirmed: "
                    f"{session.dest_converted}")
    else:
        logger.error("Destage folder for converted images does not exist")
        sys.exit(1)

    if session.dest_processed.is_dir():
        logger.info(f"Destage folder for processed images confirmed: "
                    f"{session.dest_processed}")
    else:
        logger.error("Destage folder for processed images does not exist")
        sys.exit(1)


def find_image_files(session):
    """Make a list of image files."""
    from lima.data import ImageFile

    # Set defaults for all ImageFile instances
    ImageFile.exps = session.exps
    ImageFile.spots_per_array = session.spots_per_array
    ImageFile.output_folder = session.output_folder
    ImageFile.destage_root = session.dest_converted / session.group_folder
    ImageFile.PATH_RE = session.PATH_RE
    ImageFile.EXPERIMENT_RE = session.EXPERIMENT_RE
    ImageFile.SUBFOLDER_PATTERN = session.SUBFOLDER_PATTERN
    ImageFile.TARGET_FILE_PATTERN = session.TARGET_FILE_PATTERN
    ImageFile.offset = session.offset

    files = [ImageFile(f) for f in sorted(
        session.input_folder.rglob(session.RAW_IMAGE))]

    total_file_size = sum(f.source_size for f in files)
    acquisitions = {f.acquisition for f in files}
    list_acquisitions = ", ".join(acquisitions)

    n_exps = len(session.exps)
    n_files = len(files)
    n_acquisitions = len(acquisitions)

    if n_files % (n_exps * n_acquisitions * session.spots_per_array):
        logger.error("Inconsistency in file list, spots per array may be "
                     "incorrect or there may be missing files")
        sys.exit(1)
    else:
        logger.info(f"{session.spots_per_array} spots per array confirmed")

    # Confirm array - experiment pairing
    # every experiment is a unique (set, array)
    n_arrays = len({(f.set, f.array) for f in files})
    if n_arrays != n_exps:
        logger.error("Each array must correspond to one and only one "
                     "experiment. Please check your experiment definition "
                     "file")
        sys.exit(1)
    logger.info(f"{n_arrays} array(s) confirmed")

    logger.info(f"{n_files} image files found and confirmed. "
                f"Total file size: {total_file_size / 1E+9:.1f} GB")
    logger.debug(f"Total file size: {total_file_size:,d} B")
    logger.info(f"{n_acquisitions} acquisitions found: {list_acquisitions}")

    session.files = files


def convert_files(session):
    """Convert all files."""
    from statistics import mean, stdev

    print("\nConverting files...")
    files_length = len(session.files)
    for i, f in enumerate(session.files, 1):
        f.convert()
        progress(i, files_length, 80)

    # Calculate compression level
    total_target_file_size = sum(f.target_size for f in session.files)
    logger.debug(f"Total converted file size: {total_target_file_size:,d} B")
    compression_rates = [f.compression_rate * 100 for f in session.files]
    mean_compression_rate = mean(compression_rates)
    sd_compression_rate = stdev(compression_rates, mean_compression_rate)
    logger.info(f"Average compression level: {mean_compression_rate:.1f} ± "
                f"{sd_compression_rate:.1f}%")


def build_CP_reqs(session):
    """Bring together required files for CellProfiler batch processing."""
    job_array_size = len({f.target.parent for f in session.files})
    CP_saveimage_folder = session.output_folder / session.CP_folder
    CP_saveimage_folder.mkdir(exist_ok=True)

    # Create filelist.txt
    filelist = [str(f.target) for f in session.files]
    filelist_path = session.init_folder / "filelist.txt"
    try:
        with open(filelist_path, "w") as f:
            f.writelines(f"{path}\n" for path in filelist)
    except Exception as e:
        logger.error(f"Cannot write filelist.txt ({e})")
        sys.exit(1)

    # Build script to create Batch_data.h5
    script_createBatchFile = session.scripts_path / "create_batch_data.sh"
    script_local_createBatchFile = session.init_folder / "create_batch_data.sh"
    var_createBatchFile = {"pipeline": str(session.pipeline_file),
                           "filelist": str(filelist_path)}
    modify_bash_script(script_createBatchFile, script_local_createBatchFile,
                       var_createBatchFile)

    # Build script to submit job array
    script_jobArray = session.scripts_path / "submit_jobs.sh"
    script_local_jobArray = session.init_folder / "submit_jobs.sh"
    var_jobArray = {"array_end": job_array_size}
    modify_bash_script(script_jobArray, script_local_jobArray, var_jobArray)

    # Build script to run CellProfiler and copy files back to scratch
    script_CellProfiler = session.scripts_path / "run_cellprofiler.sh"
    script_local_CellProfiler = session.init_folder / "run_cellprofiler.sh"
    var_CellProfiler = {"batch_data_file": session.batch_data_file,
                        "output": CP_saveimage_folder}
    modify_bash_script(script_CellProfiler, script_local_CellProfiler,
                       var_CellProfiler)

    # One script to rule them all, one script to find them, one script to bring
    # them all, and in the darkness bind them...
    script_main = session.scripts_path / "run.sh"
    shutil.copy2(script_main, session.init_folder)


def modify_bash_script(source, target, vars):
    """Create a copy of a script file while replacing variable fields."""
    with open(source, "r") as script_source:
        script_content = script_source.read()
    with open(target, "w") as script_target:
        script_content_modified = script_content.format(**vars)
        script_target.write(script_content_modified)


def clean_scratch(session, ask=True):
    """Clean up scratch userspace."""
    from os import remove
    if ask:
        print(f"This operation will erase all unsaved data in your scratch "
              f"userspace {session.scratch_userspace}.")
        input("\nPress any key to continue, Ctrl+C to abort")

    logger.info(f"Cleaning scratch userspace...")
    try:
        shutil.rmtree(session.scratch_userspace)
    except Exception as e:
        logger.warning(f"Cannot remove scratch userspace ({e})")
        sys.exit(1)

    logger.info(f"Removing batch data file...")
    try:
        remove(session.batch_data_file)
    except Exception as e:
        logger.warning(f"Cannot remove batch data file ({e}). Please remove "
                       f"it manually, otherwise other users cannot use the "
                       f"same CellProfiler pipeline.")
        sys.exit(1)


def concatenate_CSV_files(session):
    """Concatenate CSV files created by different CellProfiler instances."""
    import csv
    import itertools

    CSV_image = session.CSV_image
    CSV_object = session.CSV_objects
    concat_image_name = session.concat_image_name
    concat_object_name = session.concat_object_name

    if not session.CSV_output_folder.is_dir():
        logger.error(f"CSV folder {session.CSV_output_folder} is missing")
        sys.exit(1)

    logger.info("Concatenating files...")
    run_folders = [f for f in session.CSV_output_folder.glob("*")
                   if f.is_dir()]
    for i, run_folder in enumerate(run_folders, 1):
        concat_image = run_folder / concat_image_name
        if not concat_image.is_file():
            csv_image_list = run_folder.rglob(CSV_image)

            with open(next(csv_image_list), "r", newline="") as f:
                image_data = list(csv.reader(f))

            for csv_image in csv_image_list:
                with open(csv_image, "r", newline="") as f:
                    csv_data = csv.reader(f)
                    csv_content = list(csv_data)[1]  # Exclude header line
                    image_data.append(csv_content)

            with open(concat_image, "w") as f:
                w = csv.writer(f)
                w.writerows(image_data)

        concat_object = run_folder / concat_object_name
        if not concat_object.is_file():
            csv_object_list = run_folder.rglob(CSV_object)

            with open(next(csv_object_list), "r", newline="") as f:
                object_data = list(csv.reader(f))

            for csv_object in csv_object_list:
                with open(csv_object, "r", newline="") as f:
                    csv_data = csv.reader(f)
                    # Exclude 2 header lines
                    csv_content = itertools.islice(csv_data, 2, None)
                    for i in csv_content:
                        object_data.append(i)

            with open(concat_object, "w") as f:
                w = csv.writer(f)
                w.writerows(object_data)


def create_thumbs(session):
    """Create thumbnail images from images created by CP."""
    from pathlib import Path
    from PIL import Image, ImageEnhance  # External dependency

    logger.info(f"Creating thumbnails...")
    if session.CP_output_folder.is_dir():
        CP_output_images = list(
            session.CP_output_folder.rglob(session.CONVERTED_IMAGE))
    else:
        logger.error("No CP output detected")
        sys.exit(1)

    n_images = len(CP_output_images)
    if not n_images:
        logger.error("No CP output detected")
        sys.exit(1)

    for i, img in enumerate(CP_output_images, 1):
        image = Image.open(img)
        thumbnail_size = (int(image.width * session.resize_factor),
                          int(image.height * session.resize_factor))
        if session.CP_RAWIMAGE.match(str(img)):
            enhanced_filename = f"{img}{session.autocontrast_ending}"
            if not Path(enhanced_filename).exists():
                thumbnail_filename = f"{img}{session.autocontrast_ending}"\
                                     f"{session.thumbnail_ending}"
                enhancer = ImageEnhance.Contrast(image)
                image_enhanced = enhancer.enhance(session.enhance_factor)
                image_enhanced.save(enhanced_filename)
                image_enhanced.thumbnail(thumbnail_size, Image.LANCZOS)
                image_enhanced.save(thumbnail_filename)
        else:
            thumbnail_filename = f"{img}{session.thumbnail_ending}"
            if not Path(thumbnail_filename).exists():
                image.thumbnail(thumbnail_size, Image.LANCZOS)
                image.save(thumbnail_filename)

        progress(i, n_images, 80)


def destage_converted(session):
    """Destage converted files from scratch into group share."""
    logger.info(f"Destaging converted images")
    n_files = len(session.files)
    for i, f in enumerate(session.files, 1):
        f.destage_files()
        progress(i, n_files)


def destage_processed(session):
    """Destage files created by CellProfiler."""
    logger.info(f"Destaging CellProfiler output")
    CSV_subfolders = [d for d in session.CSV_output_folder.iterdir()
                      if d.is_dir()]
    CP_subfolders = [d for d in session.CP_output_folder.iterdir()
                     if d.is_dir()]

    n_folders_CSV = len(CSV_subfolders)
    n_folders = n_folders_CSV + len(CP_subfolders)
    destage_folder_converted = session.dest_converted / session.group_folder

    try:
        for i, src in enumerate(CSV_subfolders, 1):
            dst = destage_folder_converted / src.name
            if not dst.is_dir():
                shutil.copytree(src, dst)
            progress(i, n_folders)
        for i, src in enumerate(CP_subfolders, n_folders_CSV + 1):
            dst = session.dest_processed / src.name
            if not dst.is_dir():
                shutil.copytree(src, dst)
            progress(i, n_folders)
    except Exception as e:
        logger.error(f"Cannot destage CellProfiler output ({e})")
        sys.exit(1)


def destage_metadata(session):
    """Destage files created for or during operations."""
    logger.info(f"Destaging metadata")
    metadata_folder = session.dest_converted / session.group_folder / \
        "metadata"
    if not metadata_folder.is_dir():
        metadata_folder.mkdir()
        metadata_files = (f for f in session.init_folder.iterdir()
                          if f.is_file())
        for f in metadata_files:
            try:
                shutil.copy2(f, metadata_folder)
            except Exception as e:
                logger.warning(f"Cannot destage metadata file ({e})")


def renew_config(session):
    """Renew local configuration file."""
    import shutil

    try:
        shutil.copy2(session.config_file_global, session.lima_home)
    except Exception as e:
        logger.error(f"Cannot copy global configuration file to "
                     f"{session.lima_home} ({e})")
        sys.exit(1)
    else:
        logger.info("Local configuration file renewed")


def remove_config(session):
    """Remove local configuration file."""
    import os

    if session.config_file_user and session.config_file_user.is_file():
        try:
            os.remove(session.config_file_user)
        except Exception as e:
            logger.error(f"Cannot remove local configuration file "
                         f"{session.config_file_user} ({e})")
            sys.exit(1)
        else:
            logger.info("Local configuration file removed")
    else:
        logger.warning("No local configuration file to remove")


def show_log(session):
    """Print log file."""
    try:
        with open(session.logfile, "r") as f:
            print("{:*^80}".format(" Begin log file "))
            print(f.read())
            print("{:*^80}".format(" End log file "))
    except Exception as e:
        logger.error(f"Cannot read log file {session.logfile} ({e})")
        sys.exit(1)


def progress(counter, total, length=80):
    """
    Print a progress bar.

    Format: [###       ]  30.0%

    Use in a loop such as:
    n = len(iterable)
    length = 100
    for i, j in enumerate(iterable, 1):
        func(j)
        progress(i, n, length)
    """
    # bar_size is the size of "[] 100.0%", which is 9, subtracted from
    # total length
    bar_size = length - 9
    percentage = counter / total * 100
    bar = "#" * (counter * bar_size // total)

    print(f"\r[{bar: <{bar_size}}]{percentage: >6.1f}%", end="", flush=True)
    if percentage == 100:
        print("")
