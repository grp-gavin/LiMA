"""ImageFile class and experiment-related functions."""

import imread
import logging
import shutil
import sys
from pathlib import PurePath

logger = logging.getLogger(__name__)


class ImageFile():
    """
    Class to perform file operations prior to LiMA analysis.

    Attributes:
    File-related
    self.source: Full path of the source file
    self.target: Full path of the converted file
    self.destage: Full path of the destaged file
    self.source_size: Size of the source file in bytes
    self.target_size: Size of the converted file in bytes

    Microscopy-related
    self.spot: Raw spot Number
    self.acquisition: Acquisition name
    self.array: Array number in the given set
    self.set: Run set
    self.spots_per_array: Global spots per array

    Others
    self.compression_rate: Compression rate after conversion

    """

    exps = None
    spots_per_array = None
    output_folder = None
    destage_root = None
    PATH_RE = None
    EXPERIMENT_RE = None
    SUBFOLDER_PATTERN = None
    TARGET_FILE_PATTERN = None
    offset = 0

    def __init__(self, source):
        """Initialize."""
        self.source = source.resolve()
        self.source_size = self.source.stat().st_size
        self.target = None
        self.target_size = 0

        self.confirm()
        self.assign_experiment()
        self.define_paths()

    def convert(self):
        """
        Convert the raw image into an ome.tif.

        This method also corrects for an offset difference introduced
        by ScanR for some reason.
        """
        if not self.target_folder.is_dir():
            try:
                self.target_folder.mkdir(parents=True)
            except Exception as e:
                logger.error(f"Cannot create destination folder tree in "
                             f"{self.target_folder} ({e})")
                sys.exit(1)

        if not self.target.is_file():
            try:  # Image conversion based on Volker's function
                image_data, meta_data = imread.imread(self.source,
                                                      return_metadata=True)
                image_data -= self.offset
                imread.imsave(self.target, image_data, metadata=meta_data)
                shutil.copystat(self.target, self.source)
            except Exception as e:
                logger.warning(f"Cannot convert {self.source} ({e})")

        self.target_size = self.target.stat().st_size
        self.compression_rate = self.target_size / self.source_size

    def destage_files(self):
        """Copy converted file back to the group share."""
        if not self.destage.is_file():
            try:
                self.destage.parent.mkdir(parents=True, exist_ok=True)
                shutil.copy2(self.target, self.destage)
            except Exception as e:
                logger.error(f"Cannot save {self.destage} ({e})")
                sys.exit(1)

    def confirm(self):
        """Confirm image file presence and format."""
        if not self.source.is_file:
            logger.error(f"Image file not found: {self.source}")
            sys.exit(1)
        else:
            m = self.PATH_RE.match(str(self.source))
            if m is None:
                logger.error(f"Image file name does not match scanR format: "
                             f"{self.source}")
                sys.exit(1)
            else:
                self.root = PurePath(m["root"])
                self.spot = int(m["spot"])
                self.acquisition = m["acquisition"]
                self.array = (self.spot - 1) // self.spots_per_array

    def assign_experiment(self):
        """Assign the image file to an experiment."""
        for exp_name, exp_metadata in self.exps.items():
            if (self.root, self.array) == (exp_metadata["folder"],
                                           exp_metadata["array"]):
                self.experiment = exp_name
                self.set = exp_metadata["set"]

    def define_paths(self):
        """Define relevant paths."""
        # restart spot numbering after every spots_per_array spots
        spot_adj = ((self.spot - 1) % self.spots_per_array) + 1
        chip_name = self.EXPERIMENT_RE.match(self.experiment)["chip"]
        subfolder_info = {"spot": spot_adj,
                          "chip": chip_name}
        target_file_info = {"experiment": self.experiment,
                            "spot": spot_adj,
                            "chip": chip_name,
                            "acquisition": self.acquisition}

        subfolders = self.SUBFOLDER_PATTERN.format(**subfolder_info)
        f_target = self.TARGET_FILE_PATTERN.format(**target_file_info)

        self.target_folder = self.output_folder / self.experiment / subfolders
        self.destage_folder = self.destage_root / self.experiment / subfolders
        self.target = self.target_folder / f_target
        self.destage = self.destage_folder / f_target


def set_up_experiments(session):
    """Read experiment definition file and populate experiment list."""
    import yaml
    from datetime import date

    # Parse experiment definition file
    session.exp_def_path = session.input_folder / \
        session.experiment_definition_file
    try:
        with open(session.exp_def_path, "r") as f:
            exp_def = yaml.safe_load(f)
    except Exception as e:
        logger.error(f"Cannot access experiment definition file ({e})")
        sys.exit(1)
    else:
        logger.debug(f"Experiment definition file: {session.exp_def_path}")

    # Group folders and arrays into experiments
    session.exps = build_exp_dict(exp_def, session.input_folder)
    n_exps = len(session.exps)

    # Confirm experiment name format
    matched = [session.EXPERIMENT_RE.match(e) for e in session.exps.keys()]
    if None in matched:
        logger.error("Experiment names do not match the pattern")
        sys.exit(1)
    session.runs = {int(m["run"]) for m in matched}

    # Confirm that the number of experiments is just right
    if len(session.runs) != n_exps:
        logger.error("Each experiment must correspond to one and only one run")
        sys.exit(1)

    # Build group folder based on the first and the last run numbers
    run_min = min(session.runs)
    run_max = max(session.runs)
    n_run = len(session.runs)
    run_range = run_max - run_min + 1
    if run_range != n_run:
        logger.warning("Run numbers are not sequential")

    group_folder_info = {"analysis_date": date.today().strftime("%y-%m-%d"),
                         "run_first": run_min,
                         "run_last": run_max}

    session.group_folder = session.GROUP_FOLDER_PATTERN.format(
        **group_folder_info)
    session.output_folder = session.scratch_folder / session.group_folder
    logger.info(f"Output folder: {session.output_folder}")

    # Experiment design table
    logger.info(f"{n_exps} experiment(s) found and confirmed:")
    print_experiment_table(session.exps)


def print_experiment_table(exp_dict):
    """Print a list of experiments and relevant information."""
    for i, (exp_name, exp_metadata) in enumerate(exp_dict.items(), 1):
        exp_set_index = exp_metadata["set"] + 1
        array_index = exp_metadata["array"] + 1
        folder = exp_metadata["folder"]
        print(" ", end="")
        logger.info(f"{i}) Set {exp_set_index} | Array {array_index} | "
                    f"{exp_name} | {folder}")


def build_exp_dict(exp_def, input_folder):
    """Build experiment dictionary."""
    exps = {}
    try:
        for exp_set_index, (exp_set, exp_names) in enumerate(exp_def.items()):
            exp_folder = input_folder / exp_set
            exp_data_folder = exp_folder / "data"
            if exp_data_folder.is_dir():
                exp_dict = {exp_name: {"set": exp_set_index,
                                       "array": i,
                                       "folder": exp_folder}
                            for i, exp_name in enumerate(exp_names)}
                exps.update(exp_dict)
            else:
                logger.error(f"Cannot find data folder for {exp_set}")
                sys.exit(1)
    except Exception as e:
        logger.error(f"Cannot decode experiment definition file ({e})")
        sys.exit(1)

    return exps
