# Copyright (C) 2017 Cihan Erkut

"""Main function is defined here."""

import logging

logger = logging.getLogger(__name__)

# Update for every new release
__VERSION__ = "1.2.1"

# Essential paths, do not change unless absolutely necessary
LOGGING_CONFIG = "logging.conf"
LIMA_CONFIG = "lima.conf"
LIMA_HOME = "~/.lima"


def main():
    """Run main routine."""
    from lima.session import Session, parse_arguments

    session = Session(LOGGING_CONFIG, LIMA_CONFIG, LIMA_HOME, __VERSION__)
    args = parse_arguments(session)
    args.func(session, args)
    logging.shutdown()


if __name__ == "__main__":
    main()
