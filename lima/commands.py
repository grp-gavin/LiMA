# Copyright (C) 2017 Cihan Erkut

"""Commands."""

import logging
import sys

logger = logging.getLogger(__name__)


def prep(session, args):
    """Initialize session, transfer and convert files."""
    from datetime import datetime
    from pathlib import Path
    from lima.data import set_up_experiments
    from lima.files import confirm_folders, find_image_files, convert_files,\
        build_CP_reqs
    from lima.session import confirm_jobID, update_session_info

    # Start up
    confirm_jobID(session)
    session.init_moment = datetime.now()
    logger.info(f"Session initialized by {session.user}")
    print("")

    # Update session attributes from command line arguments
    session.input_folder = Path(args.INPUT).resolve()
    session.pipeline_file = Path(args.PIPELINE).resolve()
    session.scratch_root = Path(args.scratch)
    session.spots_per_array = args.spots
    session.experiment_definition_file = args.expdef

    confirm_folders(session)
    set_up_experiments(session)
    find_image_files(session)

    # Give the user a chance to cancel before things get serious
    input("\nPress any key to continue, Ctrl+C to abort")

    # Carry out file operations
    convert_files(session)
    build_CP_reqs(session)

    # Save session info
    update_session_info(session)


def post(session, args):
    """Concatenate CSV files, create thumbnails, save output."""
    import pickle
    from lima.files import clean_scratch, create_thumbs, destage_metadata,\
        destage_converted, destage_processed, concatenate_CSV_files
    from lima.session import confirm_jobID, update_session_info

    confirm_jobID(session)

    # Load session from file
    if session.session_file.is_file():
        with open(session.session_file, "rb") as f:
            session = pickle.load(f)
    else:
        logger.error("Session file is missing")
        sys.exit(1)

    session.CSV_output_folder = session.output_folder / session.CSV_folder
    session.CP_output_folder = session.output_folder / session.CP_folder

    if args.no_concat:
        logger.info("Skipping concatenation")
    else:
        concatenate_CSV_files(session)

    if args.no_thumb:
        logger.info("Skipping thumbnail generation")
    else:
        create_thumbs(session)

    if args.no_save:
        logger.info("Skipping destaging")
    else:
        destage_converted(session)
        destage_processed(session)
        destage_metadata(session)

    if args.no_cleanup:
        logger.info("Skipping scratch space cleanup")
        print("Do not forget to clean up scratch space via:")
        print("slima setup --clean-scratch")
    else:
        clean_scratch(session, ask=False)

    update_session_info(session)


def setup(session, args):
    """Configuration-related operations."""
    import pickle
    from lima.files import clean_scratch, renew_config, remove_config, show_log
    from lima.session import print_session_info

    if len(sys.argv) == 2:
        print("Use lima setup --help to see options")
        return

    if args.clean_scratch:
        clean_scratch(session)

    if args.renew_config:
        renew_config(session)

    if args.remove_config:
        remove_config(session)

    if args.show_log:
        show_log(session)

    if args.session_file:
        try:
            with open(args.session_file, "rb") as f:
                session = pickle.load(f)
        except Exception:
            print("No session file found, showing base configuration")
            print_session_info(session)
        else:
            print_session_info(session, args.verbose)
