# Thomas Hoffmann & Cihan Erkut, EMBL, 2018/04/05

easyblock = 'PythonPackage'

name = 'LiMA'
version = '1.2.1'
versionsuffix = '-Python-%(pyver)s'
options = {'modulename': 'lima'}

toolchain = {'name': 'foss', 'version': '2017b'}

description = """This is a file transfer, image compression and analysis tool designed for LiMA
experiments performed on the cube4 system. It performs file operations on images
acquired via automated microscopy in a liposome microarray (LiMA) experiment.
It is designed to run on EMBL High-Performance Computing Cluster under the
SLURM scheduler."""
homepage = 'https://git.embl.de/grp-gavin/LiMA'
documentation = 'https://git.embl.de/grp-gavin/LiMA/wikis/home'

source_urls = ['https://git.embl.de/grp-gavin/%(name)s/-/archive/release_%(version)s/']
sources = [{'download_filename': '%(name)s-release_%(version)s.tar.gz',
            'filename': '%(name)s-release_%(version)s.tar.gz'}]


# Install dependencies
PythonVersion = '3.6.4'
dependencies = [
    ('Python', PythonVersion),
    ('libjpeg-turbo', '1.5.3'),
    ('LibTIFF', '4.0.9'),
]
exts_defaultclass = 'PythonPackage'
use_pip = True
exts_list = [
    ('PyYAML', '3.12', {
        'modulename': 'yaml',
        'source_urls': ['https://files.pythonhosted.org/packages/source/p/pyyaml/'],
    }),
    ('olefile', '0.45.1', {
        'source_urls': ['https://files.pythonhosted.org/packages/source/o/olefile/'],
        'source_tmpl': 'olefile-%(version)s.zip',
    }),
    ('Pillow', '5.1.0', {
        'modulename': 'PIL',
        'source_urls': ['https://files.pythonhosted.org/packages/source/p/pillow/'],
    }),
    ('imread', '0.6.1', {
        'source_urls': ['https://files.pythonhosted.org/packages/source/i/imread/'],
    }),
]

# add scripts to PATH and make sure they are executable
modextrapaths = {'PATH': 'lib/python%(pyshortver)s/site-packages/lima/scripts'}
postinstallcmds = [
    "chmod 755 %(installdir)s/lib/python%(pyshortver)s/site-packages/lima/scripts/*",
    "sed -i.bak 's/LIMA_MODULE_VERSION/1.2.0-foss-2017b-Python-3.6.4/' %(installdir)s/lib/python%(pyshortver)s/site-packages/lima/scripts/slima.sh",
    "rm %(installdir)s/lib/python%(pyshortver)s/site-packages/lima/scripts/slima.sh.bak"]

# set the slima and jstat aliases
modextravars ={
    'SLURM_TIMELIMIT': '1:00:00',
}
modaliases = {
 'jstat': 'squeue -u \\$USER',
 'slima': 'srun slima.sh'
}

sanity_check_paths = {
    'files': [],
    'dirs': ['lib/python%(pyshortver)s/site-packages/', 'lib/python%(pyshortver)s/site-packages/lima/scripts'],
}

moduleclass = 'bio'
